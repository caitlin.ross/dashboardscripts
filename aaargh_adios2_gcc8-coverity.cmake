# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_COMMAND "cov-build --dir cov-int make")
set(CTEST_BUILD_FLAGS "-k 0")
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(CTEST_BUILD_NAME "el7-x86_64-gcc8+coverity")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load gnu8)
env_module(load hdf5)
env_module(load py2-numpy)

set(ENV{PATH} "/opt/coverity/cov-analysis-linux64-2017.07/bin:$ENV{PATH}")
set(dashboard_do_test OFF)
set(dashboard_track "Analysis")

set(ENV{CC}  gcc)
set(ENV{CXX} g++)
set(ENV{FC}  gfortran)

set(dashboard_cache "
ZFP_ROOT:PATH=$ENV{HOME}/Dashboards/Support/zfp/0.5.3
PYTHON_EXECUTABLE:FILEPATH=/usr/bin/python2.7
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)

message("Collecting Coverity scan results")
execute_process(
  COMMAND ${CMAKE_COMMAND} -E tar cvfJ ${CTEST_BUILD_NAME}.tar.xz -- cov-int
  WORKING_DIRECTORY ${CTEST_DASHBOARD_ROOT}/ADIOS2-build
)

message("Determining source version")
execute_process(
  COMMAND git describe
  WORKING_DIRECTORY ${CTEST_DASHBOARD_ROOT}/ADIOS2
  OUTPUT_VARIABLE adios2_version
)

message("Uploading Coverity scan results")
execute_process(
  COMMAND curl
    --form token=$ENV{ADIOS2_COVERITY_SUBMIT_TOKEN}
    --form email=chuck.atkins@kitware.com
    --form version="${adios2_version}"
    --form description="Nightly NoMPI"
    --form file=@${CTEST_BUILD_NAME}.tar.xz
    "https://scan.coverity.com/builds?project=ornladios%2FADIOS2"
  WORKING_DIRECTORY ${CTEST_DASHBOARD_ROOT}/ADIOS2-build
)
