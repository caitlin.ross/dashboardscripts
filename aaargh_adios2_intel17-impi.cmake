# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k 0")
set(TEST_TEST_ARGS PARALLEL_LEVEL 4)
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(CTEST_BUILD_NAME "el7-x86_64-intel17-impi")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load intel/17.0.8.262)
env_module(load py3-numpy)
env_module(load impi/2017.4.262)
env_module(load phdf5)
env_module(load py3-mpi4py)

set(ENV{CC}  icc)
set(ENV{CXX} icpc)
set(ENV{FC}  ifort)

set(dashboard_cache "
ZFP_ROOT:PATH=$ENV{HOME}/Dashboards/Support/zfp/0.5.3
PYTHON_EXECUTABLE:FILEPATH=/usr/bin/python3.4
MPIEXEC_MAX_NUMPROCS:STRING=16
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
