# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc112.fsffrance.org")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j20")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 20)

set(CTEST_BUILD_NAME "el7-pwr8-pgi18.10")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

set(PGIPREFIX "/opt/cfarm/pgi/linuxpower/18.10")
set(ENV{PATH}            "${PGIPREFIX}/bin:$ENV{PATH}")
set(ENV{LD_LIBRARY_PATH} "${PGIPREFIX}/lib:$ENV{LD_LIBRARY_PATH}")

set(ENV{CC}  pgcc)
set(ENV{CXX} pgc++)
set(ENV{FC}  pgfortran)

set(ENV{CXXFLAGS} --brief_diagnostics)

set(dashboard_cache "
ADIOS2_USE_Python:BOOL=OFF
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
