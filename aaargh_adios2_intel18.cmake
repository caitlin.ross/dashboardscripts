# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k 0")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 36)
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(CTEST_BUILD_NAME "el7-x86_64-intel18")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load intel/18.0.5.274)
env_module(load hdf5)
env_module(load py3-numpy)

set(ENV{CC}  icc)
set(ENV{CXX} icpc)
set(ENV{FC}  ifort)

set(dashboard_cache "
ADIOS2_USE_MPI:BOOL=OFF
ZFP_ROOT:PATH=$ENV{HOME}/Dashboards/Support/zfp/0.5.3
PYTHON_EXECUTABLE:FILEPATH=/usr/bin/python3.4
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
