# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc135.fsffrance.org")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j32")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 32)

set(CTEST_BUILD_NAME "el7-pwr9-xl16.1.1")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

set(XLCPREFIX "/opt/ibm/xlC/16.1.1")
set(XLFPREFIX "/opt/ibm/xlf/16.1.1")

set(ENV{CC} "${XLCPREFIX}/bin/xlc")
set(ENV{CXX} "${XLCPREFIX}/bin/xlC")
set(ENV{FC} "${XLFPREFIX}/bin/xlf")

set(dashboard_cache "
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
