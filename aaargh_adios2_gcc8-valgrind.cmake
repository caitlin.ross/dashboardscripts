# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k 0")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 36)
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(CTEST_BUILD_NAME "el7-x86_64-gcc8+valgrind")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")


find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load gnu8)
env_module(load hdf5)
env_module(load py2-numpy)

env_module(load valgrind)
find_program(CTEST_MEMORYCHECK_COMMAND valgrind)
set(dashboard_track "Analysis")
set(dashboard_do_test OFF)
set(dashboard_do_memcheck ON)

set(ENV{CC}  gcc)
set(ENV{CXX} g++)
set(ENV{FC}  gfortran)

set(dashboard_cache "
ZFP_ROOT:PATH=$ENV{HOME}/Dashboards/Support/zfp/0.5.3
PYTHON_EXECUTABLE:FILEPATH=/usr/bin/python2.7
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
