# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k 0")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 36)
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(CTEST_BUILD_NAME "el7-x86_64-clang-8.0.0+msan")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

find_package(EnvModules REQUIRED)
env_module(purge)

set(CTEST_MEMORYCHECK_TYPE "MemorySanitizer")
set(dashboard_do_memcheck ON)
set(dashboard_track "Analysis")

set(ENV{CC}  /opt/clang/8.0.0/bin/clang)
set(ENV{CXX} /opt/clang/8.0.0/bin/clang++)

set(dashboard_cache "
CMAKE_C_FLAGS=-fsanitize=memory -fno-omit-frame-pointer
CMAKE_CXX_FLAGS=-fsanitize=memory -fno-omit-frame-pointer

ADIOS2_USE_Fortran:STRING=OFF
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
