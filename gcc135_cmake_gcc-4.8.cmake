# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc135.fsffrance.org")
set(CTEST_BUILD_NAME "CentOS7-ppc64le_GCC-4.8")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-k -j32")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 32)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC 4.8")
set(dashboard_model "Nightly")

set(ENV{CC}  "/usr/bin/gcc")
set(ENV{CXX} "/usr/bin/g++")
set(ENV{FC}  "/usr/bin/gfortran")
set(dashboard_cache "
CMAKE_USE_SYSTEM_LIBRARIES:BOOL=ON
CMAKE_USE_SYSTEM_LIBRARY_LIBARCHIVE:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_ZSTD:BOOL=OFF

# Needed when dealing with high levels of parallelism
CMake_TEST_CMakeOnly.LinkInterfaceLoop_TIMEOUT:STRING=300
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
