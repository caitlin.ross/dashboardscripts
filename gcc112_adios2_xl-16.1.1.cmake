# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc112.fsffrance.org")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j20")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 20)

set(CTEST_BUILD_NAME "el7-pwr8-xl16.1.1")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

set(XLCPREFIX "/opt/ibm/xlC/16.1.1")
set(XLFPREFIX "/opt/ibm/xlf/16.1.1")
set(ENV{PATH}            "${XLCPREFIX}/bin:${XLFPREFIX}/bin:$ENV{PATH}")
set(ENV{LD_LIBRARY_PATH} "${XLCPREFIX}/lib:${XLFPREFIX}/lib:$ENV{LD_LIBRARY_PATH}")

set(ENV{CC}  xlc)
set(ENV{CXX} xlc++)
set(ENV{FC}  xlf)
set(ENV{CFLAGS} -qmaxmem=-1)
set(ENV{CXXFLAGS} -qmaxmem=-1)

set(dashboard_cache "
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
