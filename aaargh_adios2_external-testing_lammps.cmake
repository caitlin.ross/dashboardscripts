# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j72")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 1)

set(CTEST_BUILD_NAME "el7-x86_64-gcc8-openmpi3-lammps")
set(CTEST_DASHBOARD_ROOT ${CMAKE_CURRENT_BINARY_DIR}/${CTEST_BUILD_NAME})
set(dashboard_model Nightly)

find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load gnu8)
env_module(load openmpi3)
env_module(load phdf5)
env_module(load netcdf)
env_module(load netcdf-fortran)
env_module(load pnetcdf)
env_module(load py2-numpy)
env_module(load py2-mpi4py)

set(ENV{CC}  gcc)
set(ENV{CXX} g++)
set(ENV{FC}  gfortran)

set(dashboard_cache "
ADIOS2_EXTERNAL_TESTING_LAMMPS:BOOL=ON
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2-testing/adios2_external-testing_common.cmake)
