# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc135.fsffrance.org")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j32")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 32)

set(CTEST_BUILD_NAME "el7-pwr9-pgi18.10")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

set(PGIPREFIX "/opt/cfarm/pgi/linuxpower/18.10")
set(ENV{CC}  "${PGIPREFIX}/bin/pgcc")
set(ENV{FC}  "${PGIPREFIX}/bin/pgfortran")
set(ENV{F90} "${PGIPREFIX}/bin/pgf90")
set(ENV{F77} "${PGIPREFIX}/bin/pgf77")
set(ENV{CPP} "${PGIPREFIX}/bin/pgcc -Mcpp")
set(ENV{CXX} "${PGIPREFIX}/bin/pgc++")

set(ENV{CXXFLAGS} --brief_diagnostics)

set(dashboard_cache "
ADIOS2_USE_Python:BOOL=OFF
")

include(${CMAKE_CURRENT_LIST_DIR}/adios2/adios_common.cmake)
