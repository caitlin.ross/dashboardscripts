#!/bin/bash

# This retrievs the directory of the currently running script in a way that
# should work across GNU and Non-GNU environments alike (Linux, Apple, AIX,
# etc.)
SCRIPT_DIR=$(perl -MCwd -e 'print Cwd::abs_path shift' $(dirname ${BASH_SOURCE}))

# Update ourselves and re-run
cd "${SCRIPT_DIR}"
git log --pretty=format:"%h %aI [%an] %s" | head -1
if [ "${DASHBOARD_SCRIPTS_SKIP_UPDATE}" != "1" ]
then
  cd "${SCRIPT_DIR}"
  if git pull --ff-only
  then
    git submodule update --init --recursive
  fi
  export DASHBOARD_SCRIPTS_SKIP_UPDATE=1
  exec "${BASH_SOURCE}" "$@"
  exit $?
fi

# Source any site-specific variables or scripts
if [ -f ${HOME}/.dashboard ]
then
  source ${HOME}/.dashboard
fi

CTEST=${HOME}/common/cmake/latest/bin/ctest

# Make sure our tmp direcotry is on a RAM disk
export TMPDIR=/dev/shm/${USER}/tmp
mkdir -p ${TMPDIR}

######################################################################
# CMake
######################################################################
if [ "${DASHBOARD_SCRIPTS_SKIP_CMAKE}" != "1" ]
then

LOG_DIR=${HOME}/Dashboards/CMake/Logs
BASE_DIR=${HOME}/Dashboards/CMake
mkdir -p ${LOG_DIR} ${BASE_DIR}
pushd ${BASE_DIR}

# Intel compilers
for INTEL_VERSION in 16.0.{0,1,2,3,4} 17.0.{0,1,2,3,4,5,6,7,8} 18.0.{0,1,2,3,5} 19.0.{0,1,2,3,4}
do
  ${CTEST} -VV \
    -S ${SCRIPT_DIR}/aaargh_cmake_intel.cmake \
    -DINTEL_VERSION=${INTEL_VERSION} 2>&1 | \
  tee ${LOG_DIR}/aaargh_cmake_intel-${INTEL_VERSION}.log
done

# Coverity scan
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_cmake_coverity.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_cmake_coverity.log

fi
######################################################################
# ADIOS2
######################################################################
if [ "${DASHBOARD_SCRIPTS_SKIP_ADIOS2}" != "1" ]
then

LOG_DIR=${HOME}/Dashboards/ADIOS2/Logs
BASE_DIR=${HOME}/Dashboards/ADIOS2
mkdir -p ${LOG_DIR} ${BASE_DIR}
pushd ${BASE_DIR}

${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_intel17.cmake 2>&1         | \
  tee ${LOG_DIR}/aaargh_adios2_intel17.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_intel17-impi.cmake 2>&1    | \
  tee ${LOG_DIR}/aaargh_adios2_intel17-impi.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_intel18.cmake 2>&1         | \
  tee ${LOG_DIR}/aaargh_adios2_intel18.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_intel18-mvapich.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_adios2_intel18-mvapich.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_intel19.cmake 2>&1         | \
  tee ${LOG_DIR}/aaargh_adios2_intel19.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_intel19-mpich.cmake 2>&1   | \
  tee ${LOG_DIR}/aaargh_adios2_intel19-mpich.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_gcc8.cmake 2>&1            | \
  tee ${LOG_DIR}/aaargh_adios2_gcc8.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_gcc8-openmpi.cmake 2>&1    | \
  tee ${LOG_DIR}/aaargh_adios2_gcc8-openmpi.log

${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_gcc8-gcov.cmake 2>&1         | \
  tee ${LOG_DIR}/aaargh_adios2_gcc8-gcov.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_gcc8-openmpi-gcov.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_adios2_gcc8-openmpi-gcov.log
# ${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_gcc8-coverity.cmake 2>&1 | \
#   tee ${LOG_DIR}/aaargh_adios2_gcc8-coverity.log

${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_clang-asan.cmake 2>&1            | \
  tee ${LOG_DIR}/aaargh_adios2_clang-asan.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_clang-msan.cmake 2>&1            | \
  tee ${LOG_DIR}/aaargh_adios2_clang-msan.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_gcc8-valgrind.cmake 2>&1         | \
  tee ${LOG_DIR}/aaargh_adios2_gcc8-valgrind.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_gcc8-openmpi-valgrind.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_adios2_gcc8-openmpi-valgrind.log

fi
######################################################################
# ADIOS2 External Testing
######################################################################
if [ "${DASHBOARD_SCRIPTS_SKIP_ADIOS2_EXTERNAL}" != "1" ]
then

LOG_DIR=${HOME}/Dashboards/ADIOS2-External/Logs
BASE_DIR=${HOME}/Dashboards/ADIOS2-External
mkdir -p ${LOG_DIR} ${BASE_DIR}
pushd ${BASE_DIR}

${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_external-testing_grayscott.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_adios2_external-testing_grayscott.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_external-testing_heat2d.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_adios2_external-testing_heat2d.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_external-testing_lammps.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_adios2_external-testing_lammps.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_adios2_external-testing_pio.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_adios2_external-testing_pio.log

fi
