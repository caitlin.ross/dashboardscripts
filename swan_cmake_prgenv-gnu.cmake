# Client maintainer: chuck.atkins@kitware.com
if("$ENV{CRAYPE_LINK_TYPE}" STREQUAL "")
  set(link_type static)
else()
  set(link_type $ENV{CRAYPE_LINK_TYPE})
endif()

set(CTEST_SITE "swan.cray.com")
set(CTEST_BUILD_NAME "CrayLinux-CrayPE-GNU-${link_type}")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-k -j8")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/PrgEnv GNU ${link_type}")
set(dashboard_model Nightly)

find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load modules)
env_module(load craype)
env_module(load PrgEnv-gnu)
env_module(load craype-sandybridge)

set(ENV{CC}  cc)
set(ENV{CXX} CC)
set(ENV{FC}  ftn)

set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 8
  EXCLUDE
    Qt4Deploy # Compiler wrappers do squirely things with external dependencies
)

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC 4.8/CMake-build/bin

CMake_TEST_FindEnvModules:BOOL=ON
CMake_TEST_FindGTK2:BOOL=ON

CMake_TEST_Qt4:BOOL=OFF
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
