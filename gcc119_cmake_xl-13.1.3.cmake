# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc119.fsffrance.org")
set(CTEST_BUILD_NAME "AIX-7.2_XL-13.1.3")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j16")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/XL_13.1.3")
set(dashboard_model "Nightly")

set(ENV{CC}  /opt/IBM/xlC/13.1.3/bin/xlc)
set(ENV{CXX} /opt/IBM/xlC/13.1.3/bin/xlC)
set(ENV{FC}  /opt/IBM/xlf/15.1.3/bin/xlf)

set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 16
  EXCLUDE
  "^(CTestTestTimeout|CTestTestRerunFailed|Server)$"
    # CTestTestTimeout: fails too often
    # CTestTestRerunFailed: depends on CTestTestTimeout
    # Server: "cmake -E server" has an assertion failure in this test,
    #         but it can only be reproduced on AIX.  Disable pending
    #         further investigation.
)

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC_7.2.0/CMake-build/bin
CMake_TEST_CMakeOnly.AllFindModules_NO_VERSION:STRING=GETTEXT
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
