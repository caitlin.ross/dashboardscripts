#!/bin/bash

# This retrievs the directory of the currently running script in a way that
# should work across GNU and Non-GNU environments alike (Linux, Apple, AIX,
# etc.)
SCRIPT_DIR=$(perl -MCwd -e 'print Cwd::abs_path shift' $(dirname ${BASH_SOURCE}))

# Update ourselves and re-run
cd "${SCRIPT_DIR}"
git log --pretty=format:"%h %aI [%an] %s" | head -1
if [ "${DASHBOARD_SCRIPTS_SKIP_UPDATE}" != "1" ]
then
  if git pull --ff-only
  then
    git submodule update --init --recursive
  fi
  export DASHBOARD_SCRIPTS_SKIP_UPDATE=1
  exec "${BASH_SOURCE}" "$@"
  exit $?
fi

CTEST=${HOME}/common/cmake/latest/bin/ctest

# Make sure our tmp direcotry is on a RAM disk
export TMPDIR=/tmp/${USER}
mkdir -p ${TMPDIR}

######################################################################
# CMake
######################################################################
if [ "${DASHBOARD_SCRIPTS_SKIP_CMAKE}" != "1" ]
then

LOG_DIR=${HOME}/Dashboards/CMake/Logs
BASE_DIR=/lus/snx11029/${USER}/Dashboards/CMake
mkdir -p ${LOG_DIR} ${BASE_DIR}
pushd ${BASE_DIR}

${CTEST} -VV -S ${SCRIPT_DIR}/swan_cmake_gcc-4.8.cmake 2>&1      | \
  tee ${LOG_DIR}/swan_cmake_gcc-4.8.log

export CRAYPE_LINK_TYPE=dynamic
${CTEST} -VV -S ${SCRIPT_DIR}/swan_cmake_prgenv-crayclassic.cmake 2>&1 | \
  tee ${LOG_DIR}/swan_cmake_prgenv-crayclassic.log
${CTEST} -VV -S ${SCRIPT_DIR}/swan_cmake_prgenv-cray.cmake 2>&1        | \
  tee ${LOG_DIR}/swan_cmake_prgenv-cray.log
${CTEST} -VV -S ${SCRIPT_DIR}/swan_cmake_prgenv-pgi.cmake 2>&1         | \
  tee ${LOG_DIR}/swan_cmake_prgenv-pgi.log
${CTEST} -VV -S ${SCRIPT_DIR}/swan_cmake_prgenv-intel.cmake 2>&1       | \
  tee ${LOG_DIR}/swan_cmake_prgenv-intel.log
${CTEST} -VV -S ${SCRIPT_DIR}/swan_cmake_prgenv-gnu.cmake 2>&1         | \
  tee ${LOG_DIR}/swan_cmake_prgenv-gnu.log

fi
######################################################################
