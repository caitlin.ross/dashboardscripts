# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc119.fsffrance.org")
set(CTEST_BUILD_NAME "AIX-7.2_GCC-7.2.0")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j16")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC_7.2.0")
set(dashboard_model "Nightly")

set(ENV{CC}  /opt/freeware/bin/gcc)
set(ENV{CXX} /opt/freeware/bin/g++)
set(ENV{FC}  /opt/freeware/bin/gfortran)

set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 16
  EXCLUDE
    LinkStatic # https://gna.org/bugs/?20944
)

set(dashboard_cache "
CURSES_NCURSES_LIBRARY:FILEPATH=IGNORE
CMake_TEST_CMakeOnly.AllFindModules_NO_VERSION:STRING=GETTEXT
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
